package br.edu.uniateneu.bolaows;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@EntityScan("br.edu.uniateneu.bolaows.model*")
@EnableJpaRepositories("br.edu.uniateneu.bolaows.repository")
@SpringBootApplication
public class BolaoWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BolaoWsApplication.class, args);
	}

}
