package br.edu.uniateneu.bolaows.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tb_rodada")
public class RodadaEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cd_rodada")
	private Long id;
	
	@Column(name = "nr_rodada")
	private Integer nrRodada;

	@ManyToOne
	@JoinColumn(name = "cd_competicao", nullable = false)
    private CampeonatoEntity campeonato;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNrRodada() {
		return nrRodada;
	}

	public void setNrRodada(Integer nrRodada) {
		this.nrRodada = nrRodada;
	}

	public CampeonatoEntity getCampeonato() {
		return campeonato;
	}

	public void setCampeonato(CampeonatoEntity campeonato) {
		this.campeonato = campeonato;
	}
	
	
}
