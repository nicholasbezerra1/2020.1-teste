package br.edu.uniateneu.bolaows.service;

import java.util.List;

import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.bolaows.model.CampeonatoEntity;
import br.edu.uniateneu.bolaows.model.EnumTipoCompeticao;
import br.edu.uniateneu.bolaows.repository.CampeonatoRepository;

@RestController
@RequestMapping("/campeonatos")
public class CampeonatoService {
	
	@Autowired
	private CampeonatoRepository campeonatoRepository;
	
	@Produces("application/json")
	@RequestMapping(value = "/todos", method = RequestMethod.GET)
	public @ResponseBody List<CampeonatoEntity> consultar() {
		return this.campeonatoRepository.findAll();
	}
	
	@GetMapping("novo")
	public CampeonatoEntity novo () {
		Integer numero = (int) Math.round(Math.random()*9999);
		CampeonatoEntity novo = new CampeonatoEntity("campeonato "+numero,numero,EnumTipoCompeticao.ESTADUAL);
		this.campeonatoRepository.save(novo);
		return novo;
	}

	//localhost:8090/campeonatos/todos
}

